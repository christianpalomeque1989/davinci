﻿using Davinci.Data.Context;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Davinci.Web.Services
{
    /// <summary>
    /// interfaz encargada de realizar validaciones y acceso a datos cada que el controlador de productos lo requiera
    /// </summary>
    public interface IProductsManagements
    {
        Task<IEnumerable<TblProducto>> ListAsync();
        Task<TblProducto> GetByIdAsync(long? IdProduct);
        Task<List<TblProducto>> GetLitsProductsByIdAsync(List<long> IdProduct);
        Task<bool> AddAsync(TblProducto product);
        Task<bool> UpdateAsync(TblProducto product);
        Task<bool> DeleteAsync(long IdProduct);
    }
}
