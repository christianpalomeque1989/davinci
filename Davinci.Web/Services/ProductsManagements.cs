﻿using Davinci.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Services
{
    public class ProductsManagements : IProductsManagements
    {
        private readonly DavinciContext _context;       

        public ProductsManagements(DavinciContext context)
        {
            _context = context;            
        }

        public async Task<bool> AddAsync(TblProducto product)
        {
            _context.TblProducto.Add(product);
            int resul =  _context.SaveChanges();

            return resul > 0;
        }

        public async Task<bool> DeleteAsync(long IdProduct)
        {
            TblProducto tblProducto = await _context.TblProducto.FindAsync(IdProduct);
            _context.TblProducto.Remove(tblProducto);
            var result = _context.SaveChanges();

            return result > 0;
        }

        public async Task<TblProducto> GetByIdAsync(long? IdProduct)
        {            
            TblProducto tblProducto = await _context.TblProducto.FindAsync(IdProduct);

            if (tblProducto == null)
                tblProducto = new TblProducto();

            return tblProducto;
        }

        public async Task<List<TblProducto>> GetLitsProductsByIdAsync(List<long> IdProduct)
        {
            List<TblProducto> tblProducto = await _context.TblProducto.Where(w=> IdProduct.Contains(w.Id)).ToListAsync();

            if (tblProducto == null)
                tblProducto = new List<TblProducto>();

            return tblProducto;
        }

        public async Task<IEnumerable<TblProducto>> ListAsync()
        {   
            return await _context.TblProducto.ToListAsync();
        }

        public async Task<bool> UpdateAsync(TblProducto product)
        {
            TblProducto tblProducto = await _context.TblProducto.FindAsync(product.Id);
            tblProducto.Nombre = product.Nombre;
            tblProducto.Precio = product.Precio;
            tblProducto.Stock = product.Stock;
            _context.Entry(tblProducto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            var result = _context.SaveChanges();

            return result > 0;
        }
    }
}
