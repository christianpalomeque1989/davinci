﻿using Davinci.Data.Context;
using Davinci.Web.Controllers.Api.Models;
using Davinci.Web.Helper;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Services
{
    public class FacturaManagement : IFacturaManagement
    {
        private readonly DavinciContext _context;
        private readonly IConvertHelper _convertHelper;
        private readonly IDetalleManagements _detalleManagements;

        public FacturaManagement(DavinciContext context, IConvertHelper convertHelper, IDetalleManagements detalleManagements)
        {
            _context = context;
            _convertHelper = convertHelper;
            _detalleManagements = detalleManagements;
        }

        public Task<long> AddAsync(DetalleResponse factura)
        {
            return AddFacturaAsync(_convertHelper.ConvertEnTblFactura(factura));
        }

        public async Task<long> AddFacturaAsync(TblFactura factura)
        {
            _context.TblFactura.Add(factura);
            _context.SaveChanges();

            return _detalleManagements.GetUltimaFactura();
        }

        public Task<bool> AddDetalleAsync(DetalleResponse factura)
        {
            return AddDetalleAsync(_convertHelper.ConvertEnTblDetalle(factura));
        }

        public async Task<bool> AddDetalleAsync(TblDetalle factura)
        {
            _context.TblDetalle.Add(factura);
            int result = _context.SaveChanges();

            return result > 0;
        }

        public async Task<bool> DeleteAsync(long idFactura)
        {
            bool elimina = false;

            List<TblDetalle> tblDetalle = await _context.TblDetalle.Where(w => w.IdFactura == idFactura).ToListAsync();

            foreach (TblDetalle item in tblDetalle)
            {
                _context.TblDetalle.Remove(item);
            }

            int result = _context.SaveChanges();

            if (result > 0)
            {
                elimina = await _detalleManagements.DeleteAsync(idFactura);
            }

            return elimina;
        }

        public async Task<FacturaResponse> GetByIdAsync(long IdFactura)
        {
            FacturaResponse tblFactura = _convertHelper.GetFacturaResponse(await _context.TblFactura.FindAsync(IdFactura));

            if (tblFactura == null)
            {
                tblFactura = new FacturaResponse();
            }

            return tblFactura;
        }

        public async Task<IEnumerable<TblFactura>> ListAsync()
        {
            return await _context.TblFactura.ToListAsync();
        }

    }
}
