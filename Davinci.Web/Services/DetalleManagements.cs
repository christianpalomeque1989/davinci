﻿using Davinci.Data.Context;
using Davinci.Web.Controllers.Api.Models;
using Davinci.Web.Helper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Services
{
    public class DetalleManagements : IDetalleManagements
    {
        private readonly DavinciContext _context;
        private readonly IConvertHelper _converterHelper;

        public DetalleManagements(DavinciContext context, IConvertHelper converterHelper)
        {
            _context = context;
            _converterHelper = converterHelper;
        }

        public async Task<bool> DeleteAsync(long idFactura)
        {
            List<TblFactura> tblFacturas = await _context.TblFactura.Where(w => w.Id == idFactura).ToListAsync();

            foreach (var item in tblFacturas)
            {
                _context.TblFactura.Remove(item);
            }

            var result = _context.SaveChanges();

            return result > 0;
        }

        public async Task<List<DetalleResponse>> GetByIdAsync(long IdFactura)
        {
            List<DetalleResponse> detalles = _converterHelper.GetDetalleResponse(await _context.TblDetalle.Where(w => w.IdFactura == IdFactura).ToListAsync());

            return detalles;
        }

        public long GetUltimaFactura()
        {
            return _context.TblFactura.Max(m => m.Id);
        }
    }
}
