﻿using Davinci.Data.Context;
using Davinci.Web.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Services
{
    /// <summary>
    /// Interfaz encargada de realizar validaciones y acceso a datos cuando el controlador de facturación lo requiera
    /// </summary>
    public interface IFacturaManagement
    {
        Task<IEnumerable<TblFactura>> ListAsync();
        Task<FacturaResponse> GetByIdAsync(long IdFactura);
        Task<long> AddAsync(DetalleResponse factura);        
        Task<bool> DeleteAsync(long IdFactura);
        Task<bool> AddDetalleAsync(DetalleResponse factura);
    }
}
