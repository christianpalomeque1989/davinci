﻿using Davinci.Web.Controllers.Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Davinci.Web.Services
{
    /// <summary>
    /// Interfaz que se encargar de validar el detalle de facturación
    /// </summary>
    public interface IDetalleManagements
    {
        Task<List<DetalleResponse>> GetByIdAsync(long IdFactura);
        long GetUltimaFactura();
        Task<bool> DeleteAsync(long IdFactura);
    }
}
