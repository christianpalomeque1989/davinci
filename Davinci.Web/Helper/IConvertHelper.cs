﻿using Davinci.Data.Context;
using Davinci.Web.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Helper
{
    /// <summary>
    /// Interfaz encargada de transormar un modelo en otro segun conveniencia.
    /// </summary>
    public interface IConvertHelper
    {
        FacturaResponse GetFacturaResponse(TblFactura tblFactura);
        List<DetalleResponse> GetDetalleResponse(List<TblDetalle> tblDetalles);
        string GetNombreCliente(long? id);
        ProductResponse GetProductResponse(long? id);
        ProductResponse GetProductResponse(TblProducto tblProducto);
        TblFactura ConvertEnTblFactura(DetalleResponse factura);
        TblDetalle ConvertEnTblDetalle(DetalleResponse factura);
    }
}
