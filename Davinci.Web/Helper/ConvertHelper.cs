﻿using Davinci.Data.Context;
using Davinci.Web.Controllers.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Helper
{
    public class ConvertHelper : IConvertHelper
    {
        private readonly DavinciContext _context;

        public ConvertHelper(DavinciContext context)
        {
            _context = context;
        }

        public TblFactura ConvertEnTblFactura(DetalleResponse factura)
        {
            return new TblFactura {Fecha = factura.fechaFactura, IdCliente = factura.IdCliente};
        }

        public TblDetalle ConvertEnTblDetalle(DetalleResponse factura)
        {
            return new TblDetalle { Cantidad = factura.Cantidad, IdFactura = factura.IdFactura, IdProducto= factura.IdProducto, Precio = factura.Precio};
        }

        public List<DetalleResponse> GetDetalleResponse(List<TblDetalle> tblDetalles)
        {
            List<DetalleResponse> result = new List<DetalleResponse>();
            foreach (var detalle in tblDetalles)
            {
                result.Add(new DetalleResponse
                {
                    Cantidad = detalle.Cantidad,
                    Id = detalle.Id,
                    IdFactura = detalle.IdFactura,
                    IdProducto = detalle.IdProducto,
                    Precio = detalle.Precio, 
                    ProductResponse = new List<ProductResponse>()
                });
            }

            return result;
        }

        public FacturaResponse GetFacturaResponse(TblFactura tblFactura)
        {
            return new FacturaResponse
            {
                Id = tblFactura.Id,
                Fecha = tblFactura.Fecha,
                IdCliente = tblFactura.IdCliente
            };
        }

        public string GetNombreCliente(long? id)
        {
            return _context.TblCliente.Find(id).NombreCompleto;
        }

        public ProductResponse GetProductResponse(long? id)
        {
            ProductResponse product = new ProductResponse();
            product = GetProductResponse(_context.TblProducto.Find(id));
            return product;
        }

        public ProductResponse GetProductResponse(TblProducto tblProducto)
        {
            ProductResponse response = new ProductResponse();
            response.Id = tblProducto.Id;
            response.Nombre = tblProducto.Nombre;
            response.Precio = tblProducto.Precio;
            response.Stock = tblProducto.Stock;
            return response;
        }
    }
}
