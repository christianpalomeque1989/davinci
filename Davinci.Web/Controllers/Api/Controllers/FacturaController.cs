﻿using Davinci.Data.Context;
using Davinci.Web.Controllers.Api.Models;
using Davinci.Web.Helper;
using Davinci.Web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Controllers.Api.Controllers
{
    /// <summary>
    /// Controlador encargado del crud de las facturas.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class FacturaController : ControllerBase
    {
        private readonly IFacturaManagement _facturaManagement;
        private readonly IDetalleManagements _detalleManagements;        
        private readonly IConvertHelper _convertHelper;

        public FacturaController(IFacturaManagement facturaManagement, IDetalleManagements detalleManagements,IConvertHelper convertHelper)
        {
            _facturaManagement = facturaManagement;
            _detalleManagements = detalleManagements;           
            _convertHelper = convertHelper;
        }

        /// <summary>
        /// Obtener una lista de Facturas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_facturaManagement.ListAsync().Result);
        }

        /// <summary>
        /// Obtener Factura por Id
        /// </summary>
        /// <param name="Id">Id de la factura</param>
        /// <returns></returns>
        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(long Id)
        {
            try
            {
                if (Id == 0)
                {
                    return BadRequest("Identificador invalido");
                }

                FacturaResponse factura = _facturaManagement.GetByIdAsync(Id).Result;

                List<DetalleResponse> detallesFacturacion = _detalleManagements.GetByIdAsync(factura.Id).Result;
                List<ProductResponse> productResponses = new List<ProductResponse>();
                
                foreach (var detalle in detallesFacturacion)
                {
                    detalle.fechaFactura = factura.Fecha;
                    detalle.NombreCliente = _convertHelper.GetNombreCliente(factura.IdCliente);
                    detalle.IdCliente = factura.IdCliente;
                    detalle.ProductResponse.Add(_convertHelper.GetProductResponse(detalle.IdProducto));                    
                }

                return Ok(detallesFacturacion);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ocurrio un error: {ex.Message}");
            }
        }

        /// <summary>
        /// Insertar una factura y detalle
        /// </summary>
        /// <param name="producto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] DetalleResponse factura)
        {
            try
            {
                if (factura == null)
                {
                    return BadRequest("Objeto nulo");
                }

                var resultadoFactura = _facturaManagement.AddAsync(factura).Result;
                factura.IdFactura = resultadoFactura;

                var resultadoDetalle = _facturaManagement.AddDetalleAsync(factura).Result;

                return Ok(resultadoDetalle);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ocurrio un error: {ex.Message}");
            }
        }

        /// <summary>
        /// Elimina una factura y su detalle
        /// </summary>
        /// <param name="producto"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] TblFactura factura)
        {
            try
            {
                if (factura.Id == 0)
                {
                    return BadRequest("Objeto nulo");
                }

                var resultado = _facturaManagement.DeleteAsync(factura.Id).Result;

                return Ok(resultado);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ocurrio un error: {ex.Message}");
            }
        }
    }
}
