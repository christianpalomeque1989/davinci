﻿using Davinci.Data.Context;
using Davinci.Web.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Davinci.Web.Controllers.Api.Controllers
{

    /// <summary>
    /// controlador encargado del crud de los productos
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {  
        private readonly IProductsManagements _productsManagements;

        public ProductsController(IProductsManagements productsManagements)
        {   
            _productsManagements = productsManagements;
        }

        /// <summary>
        /// Obtener una lista de Productos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_productsManagements.ListAsync().Result);
        }

        /// <summary>
        /// Obtener producto por Id
        /// </summary>
        /// <param name="Id">Id del producto</param>
        /// <returns></returns>
        [HttpGet("{Id}")]
        public async Task<IActionResult> Get(long Id)
        {
            try
            {
                if (Id == 0)
                {
                    return BadRequest("Identificador invalido");
                }
                
                var resultado = _productsManagements.GetByIdAsync(Id).Result;

                return Ok(resultado);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ocurrio un error: {ex.Message}");
            }
        }


        /// <summary>
        /// Insertar un producto
        /// </summary>
        /// <param name="producto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] TblProducto producto)
        {
            try
            {
                if (producto == null)
                {
                    return BadRequest("Objeto nulo");
                }

                var resultado = _productsManagements.AddAsync(producto).Result;

                return Ok(resultado);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ocurrio un error: {ex.Message}");
            }
        }

        /// <summary>
        /// Elimina un producto
        /// </summary>
        /// <param name="producto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] TblProducto producto)
        {
            try
            {
                if (producto.Id == 0)
                {
                    return BadRequest("Objeto nulo");
                }

                var resultado = _productsManagements.UpdateAsync(producto).Result;

                return Ok(resultado);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ocurrio un error: {ex.Message}");
            }
        }

        /// <summary>
        /// Elimina un producto
        /// </summary>
        /// <param name="producto"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] TblProducto producto)
        {
            try
            {
                if (producto.Id == 0)
                {
                    return BadRequest("Objeto nulo");
                }

                var resultado = _productsManagements.DeleteAsync(producto.Id).Result;

                return Ok(resultado);

            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Ocurrio un error: {ex.Message}");
            }
        }
    }
}
