﻿using System;
using System.Collections.Generic;


namespace Davinci.Web.Controllers.Api.Models
{
    public class DetalleResponse
    {
        public long Id { get; set; }
        public long? IdFactura { get; set; }
        public long? IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
        public string NombreCliente { get; set; }
        public long? IdCliente { get; set; }
        public DateTime fechaFactura { get; set; }
        public List<ProductResponse> ProductResponse { get; set; }
    }
}
