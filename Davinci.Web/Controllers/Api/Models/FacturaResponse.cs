﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Davinci.Web.Controllers.Api.Models
{
    public class FacturaResponse
    {
        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public long? IdCliente { get; set; }

        public ClienteResponse ClienteResponse { get; set; }
    }
}
