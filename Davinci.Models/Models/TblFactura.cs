﻿using System;
using System.Collections.Generic;

namespace Davinci.Data.Context
{
    public partial class TblFactura
    {
        public TblFactura()
        {
            TblDetalle = new HashSet<TblDetalle>();
        }

        public long Id { get; set; }
        public DateTime Fecha { get; set; }
        public long? IdCliente { get; set; }

        public virtual TblCliente IdClienteNavigation { get; set; }
        public virtual ICollection<TblDetalle> TblDetalle { get; set; }
    }
}
