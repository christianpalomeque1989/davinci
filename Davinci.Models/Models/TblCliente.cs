﻿using System;
using System.Collections.Generic;

namespace Davinci.Data.Context
{
    public partial class TblCliente
    {
        public TblCliente()
        {
            TblFactura = new HashSet<TblFactura>();
        }

        public long Id { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Cedula { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string NombreCompleto => $"{Nombres} {Apellidos}";
        public virtual ICollection<TblFactura> TblFactura { get; set; }
    }
}
