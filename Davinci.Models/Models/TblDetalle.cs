﻿using System;
using System.Collections.Generic;

namespace Davinci.Data.Context
{
    public partial class TblDetalle
    {
        public long Id { get; set; }
        public long? IdFactura { get; set; }
        public long? IdProducto { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }

        public virtual TblFactura IdFacturaNavigation { get; set; }
        public virtual TblProducto IdProductoNavigation { get; set; }
    }
}
