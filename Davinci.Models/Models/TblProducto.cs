﻿using System.Collections.Generic;

namespace Davinci.Data.Context
{
    public partial class TblProducto
    {
        public TblProducto()
        {
            TblDetalle = new HashSet<TblDetalle>();
        }

        public long Id { get; set; }
        public string Nombre { get; set; }
        public decimal Precio { get; set; }
        public int Stock { get; set; }

        public virtual ICollection<TblDetalle> TblDetalle { get; set; }
    }
}
