﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Davinci.Data.Context
{
    public partial class DavinciContext : DbContext
    {
        public DavinciContext()
        {
        }

        public DavinciContext(DbContextOptions<DavinciContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblCliente> TblCliente { get; set; }
        public virtual DbSet<TblDetalle> TblDetalle { get; set; }
        public virtual DbSet<TblFactura> TblFactura { get; set; }
        public virtual DbSet<TblProducto> TblProducto { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Server=SERCO5CG9263D7R\\SQLEXPRESS;Database=Davinci;Trusted_Connection=True;");
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TblCliente>(entity =>
            {
                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Cedula)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Nombres)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FechaNacimiento).HasColumnType("datetime");
            });

            modelBuilder.Entity<TblDetalle>(entity =>
            {
                entity.Property(e => e.IdFactura).HasColumnName("Id_Factura");

                entity.Property(e => e.IdProducto).HasColumnName("Id_Producto");

                entity.Property(e => e.Precio).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.IdFacturaNavigation)
                    .WithMany(p => p.TblDetalle)
                    .HasForeignKey(d => d.IdFactura)
                    .HasConstraintName("FK__TblDetall__Id_Fa__3E52440B");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.TblDetalle)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK__TblDetall__Id_Pr__3F466844");
            });

            modelBuilder.Entity<TblFactura>(entity =>
            {
                entity.Property(e => e.Fecha).HasColumnType("datetime");

                entity.Property(e => e.IdCliente).HasColumnName("Id_Cliente");

                entity.HasOne(d => d.IdClienteNavigation)
                    .WithMany(p => p.TblFactura)
                    .HasForeignKey(d => d.IdCliente)
                    .HasConstraintName("FK__TblFactur__Id_Cl__398D8EEE");
            });

            modelBuilder.Entity<TblProducto>(entity =>
            {
                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Precio).HasColumnType("decimal(10, 2)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
